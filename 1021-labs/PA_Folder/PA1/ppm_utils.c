/*************************
 *Za'Cori Ross
 *CPSC 1020 001, Sp18
 *zacorir@g.clemson.edu
 *************************/

#include "ppm_utils.h"
#include "stdbool.h"

void ckws_comments (FILE* inFileptr) {
  char c;
  char *buffer;

  FILE* fp = fopen (inFileptr, "r");
  c = fgetc (fp);
  while (c == '#' || isspace(c)) {
    if (c == '#') {
      getline (&buffer, 50, fp);
    }
    c = fgetc (fp);
  }
  ungetc (c, inFileptr);

}

header_t read_header(FILE* image_file) {
  header_t header;
  fscanf (image_file, "%s", header.MAGIC_NUMBER);
  ckws_comments (image_file);
  fscanf (image_file, "%d", &header.WIDTH);
  ckws_comments (image_file);
  fscanf (image_file, "%d", &header.HEIGHT);
  ckws_comments (image_file);
  fscanf (image_file, "%d", &header.MAX_COLOR);
  ckws_comments (image_file);
  return header;
}

void write_header(FILE* out_file, header_t header) {
  fprintf(out_file, "%s %d %d %d ",
    header.MAGIC_NUMBER, header.WIDTH, header.HEIGHT, header.MAX_COLOR);
}

image_t* read_ppm(FILE* image_file) {
  header_t header = read_header(image_file);
  image_t* image = NULL;
  if (strcmp("P3", header.MAGIC_NUMBER) == 0) {
    image = read_p3(image_file, header);
  } else if (strcmp("P6", header.MAGIC_NUMBER) == 0) {
    image = read_p6(image_file, header);
  }
  return image;
}

image_t* read_p6(FILE* image_file, header_t header) {
  int num_pixels = header.HEIGHT * header.WIDTH;
  image_t* image = (image_t*) malloc(sizeof(image_t));
  image->header = header;
  image->pixels = (pixel_t*) malloc(sizeof(pixel_t) * num_pixels);
  for(int i = 0; i < num_pixels; i++) {
    char r,g,b;
    fscanf(image_file, "%c%c%c", &r, &g, &b);
    image->pixels[i].R = (int) r;
    image->pixels[i].G = (int) g;
    image->pixels[i].B = (int) b;
  }
  return image;
}

image_t* read_p3(FILE* image_file, header_t header) {
  int num_pixels = header.HEIGHT * header.WIDTH;
  image_t* image = (image_t*) malloc(sizeof(image_t));
  image->header = header;
  image->pixels = (pixel_t*) malloc(sizeof(pixel_t) * num_pixels);
  for(int i = 0; i < num_pixels; i++) {
    int r,g,b;
    fscanf(image_file, "%d %d %d ", &r, &g, &b);
    image->pixels[i].R = r;
    image->pixels[i].G = g;
    image->pixels[i].B = b;
  }
  return image;
}

void write_p6(FILE* out_file, image_t* image) {
  header_t header = image->header;
  header.MAGIC_NUMBER[1] = '6';
  write_header(out_file, header);
  int num_pixels = image->header.HEIGHT * image->header.WIDTH;
  for(int i = 0; i < num_pixels; i++) {
    fprintf(out_file, "%c%c%c",
      (char) image->pixels[i].R,
      (char) image->pixels[i].G,
      (char) image->pixels[i].B
    );
  }
}

void write_p3(FILE* out_file, image_t* image) {
  header_t header = image->header;
  header.MAGIC_NUMBER[1] = '3';
  write_header(out_file, header);
  int num_pixels = image->header.HEIGHT * image->header.WIDTH;
  for(int i = 0; i < num_pixels; i++) {
    fprintf(out_file, "%d %d %d ",
      image->pixels[i].R,
      image->pixels[i].G,
      image->pixels[i].B
    );
  }
}

void openInputFiles (char* name, FILE* input[]) {
  char file[50];
  int i, size;
  if (strcmp (name, "average") == 0) {
    size = 10;
  }
  if (strcmp (name, "median") == 0) {
    size = 9;
  }
  sprintf (file, "%s_%03d.ppm", name, i);
  for (i = 0; i < size; i++) {
    FILE* out_file = fopen (file, "w");
    if (!out_file) {
      printf ("ERROR: File(s) could not be opened\n");
      return 1;
    }
  }
}

image_t* removeNoiseAverage (image_t* image[]) {
  int sum_r, sum_g, sum_b;
  unsigned int avg_r, avg_g, avg_b;
  int num_pixels = header.HEIGHT * header. WIDTH;
  image_t* avgImg = (image_t*)malloc(sizeof(image_t));
  avgImg->header = header;
  avgImg->pixels = (pixel_t*)malloc(sizeof(pixel_t) * num_pixels);
  for (int i = 0; i < 10; i++) {
    int r, g, b;
    fscanf (image[i], "%d %d %d ", &r, &g, &b);
    sum_r += r;
    sum_g += g;
    sum_b += b;
  }
  avg_r = sum_r / 10;
  avg_g = sum_g / 10;
  avg_b = sum_b / 10;
  avgImg->pixels.R = avg_r;
  avgImg->pixels.G = avg_g;
  avgImg->pixels.B = avg_b;
  return (avgImg);
}

image_t* removeNoiseMedian (image_t* image[]) {
  int size = 9;
  unsigned int Red[size];
  unsigned int Green[size];
  unsigned int Blue[size];
  for (int i = 0; i < size; i++) {
    unsigned int r, g, b;
    fscanf (image[i], "%d %d %d ", &r, &g, &b);
    Red[i] = r;
    Green[i] = g;
    Blue[i] = b;
  }
  sort (Red, size);
  sort (Green, size);
  sort (Blue, size);
  int num_pixels = header.HEIGHT * header.WIDTH;
  image_t* medImg = (image_t*)malloc(sizeof(image_t));
  medImg->header = header;
  medImg->pixels = (pixel_t*)malloc(sizeof(pixel_t*) * num_pixels);
  medImg->pixels.R = Red[5];
  medImg->pixels.G = Green[5];
  medImg->pixels.B = Blue[5];
  return (medImg);
}

void sort (unsigned int* arr, int n) {
  bool swapped = true;

  while (swapped == true) {
    swapped = false;
    for (int i = 0; i < n; i++) {
      if (arr[i] > arr[i + 1]) {
        swapped = true;
        swap (&arr[i], &arr[i + 1]);
      }
    }
  }
}

void swap (unsigned int* a, unsigned int* b) {
  unsigned int temp;
  temp = *a;
  *a = *b;
  *b = temp;
}
