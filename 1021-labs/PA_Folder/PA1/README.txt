/**************************
 *Za'Cori Ross
 *CPSC 1020 006, Sp18
 *zacorir@g.clemson.edu
 *************************/

 Most of my problems came when compiling my code.
I had a general grasp of what the concept of the program
was, however I had trouble keeping up with what pointers and
files went where. And i couldnt figure out why the compiler
wouldnt let me cast into R, G, and B withing the pixel struct.
I also kept receiving an error message with regards to when I
called use "img = header.HEIGHT * header.WIDTH;" saying
that it was the first time I had used header in the funciton
and that it wasnt declared.

I thought it was an interesting assignment and I enjoyed 
attempting to code it. I am just mostly frustrated because
i could not resolve those errors by my self.

