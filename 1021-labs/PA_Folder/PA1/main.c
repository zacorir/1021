/*************************
 *Za'Cori Ross
 *CPSC 1020 001, Sp18
 *zacorir@g.clemson.edu
 *************************/

#include "ppm_utils.h"

int main (int argc, char* argv[]) {
  if (strcmp (argv[1], "average") == 0) {
    FILE* average_Files[10];
    image_t* average[10];
    openInputFiles (argv[1], average_Files);
    image_t* avgImg = (image_t*) malloc(sizeof(image_t));
    for (int i = 0; i < 10; i++) {
      average[i] = read_ppm (average_Files[i]);
    }
    avgImg = removeNoiseAverage (average[10]);
    FILE* avg_out = fopen (argv[3], "w");
    write_p6 (avg_out, avgImg);
    free (avgImg->pixels);
    free (avgImg);
    free (average);
    free (avg_out);
  }
  if (strcmp (argv[1], "median") == 0) {
    FILE* median_Files[9];
    image_t* median[9];
    openInputFiles (argv[1], median_Files);
    image_t* medImg = (image_t*) malloc(sizeof(image_t));
    for (int i = 0; i < 9; i++) {
      median[i] = read_ppm (median_Files[i]);
    }
    medImg = removeNoiseMedian (median[9]);
    FILE* med_out = fopen (argv[3], "w");
    write_p3 (med_out, medImg);
    free (medImg->pixels);
    free (medImg);
    free (median);
    fclose (med_out);
  }

  return 0;
}
