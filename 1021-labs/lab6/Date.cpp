#include "Date.h"
#include <fstream>

// Just an array of strings. Access a month using:
// Date::MONTHS[0] ---> "JANUARY"
// GLOBAL TO THE CLASS
// Also, why is this const?
const string Date::MONTHS[] = {
  "JANUARY", "FEBRUARY", "MARCH", "APRIL", "MAY",
  "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER",
  "NOVEMBER", "DECEMBER"
};

// Default, just make sure the fields are set to something
Date::Date() {
  this->month = 1;
  this->day = 1;
  this->year = 1000;
}
// Destructor, end of object life cycle
Date::~Date() {}

// STATIC COMPARE
bool Date::compare(const Date& lhs, const Date& rhs) {
  if (rhs.year == lhs.year){
    return (rhs.month > lhs.month);
  }
  else{
    return (rhs.year > lhs.year);
  }
  if (rhs.year == lhs.year){
    if (rhs.month == lhs.month){
      return (rhs.day > lhs.day);
    }
    else{
      return (rhs.month > lhs.month);
    }
  }
  else{
    return (rhs.year > lhs.year);
  }
}

// Sample getter/setter
// What does this const mean?
int Date::get_month() const {
  return this->month;
}
int Date::get_day() const {
  return this->day;
}
int Date::get_year() const {
  return this->year;
}
void Date::set_month(int month) {
  this->month = month;
}
void Date::set_day(int day) {
  this->day = day;
}
void Date::set_year(int year) {
  this->year = year;
}

std::string Date::printDates(){
  stringstream ss;
  ss << setw(10) << Date::MONTHS[this->month - 1]
     << setw(3)  << this->day
     << setw(5)  << this->year << endl;
  return ss.str();
}
