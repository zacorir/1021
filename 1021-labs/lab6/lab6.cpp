#include "Date.h"
#include <algorithm>
#include <fstream>

int main(int argc, char const *argv[]){
  std::ifstream inFile(argv[1]);
  std::ofstream outFile(argv[2]);

  int num, m, d, y, i;
  inFile >> num;

  if(!(inFile && outFile)){
    std::cout<< "Error. Could not open file\n";
    return 0;
  }

  Date dates[num];
  for(i = 0; i < num; i++){
    inFile >> m;
    inFile >> d;
    inFile >> y;
    dates[i].set_month(m);
    dates[i].set_day(d);
    dates[i].set_year(y);
  }

  std::sort(dates, dates + num, Date::compare);

  for(i = 0; i < num; i++){
    outFile << dates[i].printDates();
  }

  inFile.close();
  outFile.close();
  return 0;
}
