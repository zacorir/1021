#include <stdio.h>

int main()
{
    /*Since we did not use file pointer we will use redirection to determine
     *the file name*/
    int i;
    fprintf(stdout, "P5\n 500 500 255\n");

    for(i = 0; i < 500 * 500; i++)
    {
            fprintf(stdout,"%c",  150);
    }

  return 0;
}
