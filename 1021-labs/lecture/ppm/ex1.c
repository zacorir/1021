#include <stdio.h>
#include <stdlib.h>

int main(int argc, char* argv[])
{
  int i;
  /*open a file to write to*/
  FILE* out = fopen(argv[1], "w");

  /*write the header*/
  fprintf(out, "P^\n200 200 255\n");

  /*write the pixels clemson purple 82, 45, 128 or orange 234, 106, 32*/
  for(i = 0; i < 200 * 200; i++)
  {
    fprintf(out, "%c%c%c", 82, 45, 128);
  }

  return 0;
}
