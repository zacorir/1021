#include <stdio.h>
int main()
{
	int x = 5;

	//p points to x; x = 5
	int *p = &x;

	//go to where p points and change the valaue to 6
	*p = 6;

	//the pointer q points to the pointer p which points to x
	int **q = &p; 

	//r points to the double pointer q which points to p which points to x
	int ***r = &q;

	/*What would happen if we pointed ***r directly to &p? If you try to apply
	 *indirection on all three pointers  you will get a segfault.*/
	//int ***r = &p;
	//go to where p points and get the value
	printf("%d\n", *p);

	/*go to where q points and get the value -- q points to p which
	 *is a pointer that points to x  but you will get the value in p */ 
	printf("%d\n", *q);

	/*go to where r points and get the value -- r points to q which 
	 *is a pointer that points p --  we only have one indirection so you 
	 *get the value of q*/
	printf("%d\n", *r);

	/*go to where q points, which is p, and get the value -- then go
	 *to where p points, which is x, get the value x = 6*/
	printf("%d\n", **q);

	/*go to where r points, which is q, get the value -- q points to
	 *p get the value which is an address to x*/
	printf("%d\n", **r);

	/*go to where r points, which is q, get the value -- go to q get
	 *the value, which it points p, then go to where p points which is x
	 *get the value, which is 6*/
	printf("%d\n", ***r);

	return 0;
}