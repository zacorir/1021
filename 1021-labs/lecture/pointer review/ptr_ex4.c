#include <stdio.h>
/*This program demonstrings the size of arrays and how to set a pointer equal
 *to arrays.  It also demonstrates you can access what a pointer is pointing to
 *using array notation if the pointer is pointing to an array.
 *Why would you want a pointer to a pointer.  Sometimes the pointer is small so
 *it is more efficient to pass a pointer to a function than a large array or
 *any other data structure.*/
int main()
{
    int iArr[] = {1, 2 ,3};
    int *iPtr = iArr;/*must have the address to pass the value of the first
    element */

    char cArr[] = {64, 'A' , 98};
    char *cPtr = cArr;

    /*The size of an int depends on the computers architecture.  for my laptop
     *an array is 4 bytes so 4 * 3 = 12 bytes. However, an iPtr a pointer to an
     *int is 8 bytes*/
    printf("sizeof [] = %lu \n", sizeof(iArr));
    printf("sizeof iPtr = %lu \n", sizeof(iPtr));

    /*Characters are only one byte of data so the total size of cArr is
     *3 * 1 = three bytes*/
    printf("sizeof cArr[] = %lu \n", sizeof(cArr));
    printf("sizeof cPtr = %lu \n", sizeof(cPtr));

    /*I can access these pointers using array notation or pointer arithmetic */
    printf("cPtr[0] = %c \n", cPtr[0]);
    printf("cPtr[1] = %c \n", *(cPtr+1));
    printf("cPtr[1] + 1 = %c \n", *(cPtr+1)+1);
    printf("cPtr[2] = %c \n", *(cPtr+2));

    printf("iPtr[0] = %d \n", iPtr[0]);
    printf("iPtr[1] = %d \n", *(iPtr+1));
    printf("iPtr[1] + 1 = %d \n", *(iPtr+1)+1);
    printf("iPtr[2] = %d \n", *(iPtr+2));
    return 0;
}
