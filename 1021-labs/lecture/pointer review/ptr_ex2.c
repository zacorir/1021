# include <stdio.h>

/*This program fixes the problem with ptr_ex1.c  The program passes by reference
 *rather than by value*/
void fun(int*);

int main()
{
  /*I could have created a pointer of type 'int' and pointed it to 'y' then pass
   *the pointer to fun.  However C allows us to just directly pass the address
   *of 'y'. What whould this have looked like.*/
  int y = 20;
  fun(&y);
  /*This should now print 30 rather than 20.  */
  printf("%d", y);

  return 0;
}

void fun(int *ptr)
{
    *ptr = 30;
}
