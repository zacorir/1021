#include <stdio.h>

/*This program demonstrasts the problem with passing by value rather than
 *pass by reference (pointer)*/
void fun(int x);

int main()
{
  int y = 20;
  fun(y);
  /*This line of code will print 20 rather than 30 Why????*/
  printf("%d", y);
  return 0;
}

void fun(int x)
{
    x = 30;
}
