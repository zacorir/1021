#include <stdio.h>

int main()
{

	int num1 = 5;
	int num2 = 10;
	int *ptr = &num1;
	int array[10] = {1,3,2,3,8,2,6,-1,0,9};

	/*Here I will print the address of each element of the array
	 *on my computer these addresses are 4 bytes (32 bits) apart.*/
	for(int i = 0; i < 10; i++)
	{
			printf("address of array[%d] is: %p\n", i, &array[i]);
	}

	/*This is the equivalent of num2 = num2 + *ptr.  *ptr says go
	 *to what ptr is pointing at and get the value and add it to
	 *the value of num2.*/
	num2 += *ptr;
	printf("num2 is: %d\n", num2);

	/*This is the equivalent of num2 = num2 + (*ptr + 3)  num2 at this point
	 *is equal to 15, *ptr is still pointing to num1 which has the value of 5 so
	 *add 3 to get 8 add this is to num2 (15) to get 23*/
	num2 += *ptr + 3;

	printf("num2 is: %d\n", num2);//this should print 23

	/*Now have we changed where ptr is pointing?  No!  So this should print 5*/

	printf("*ptr is: %d\n", *ptr);

	/*Now we set ptr to point to the array.  This can be done in two ways.
	 *Either works.  */
	ptr = &array[0];
	//ptr = array;

	/*This shows us that ptr is pointing to array[0]*/
	printf("*ptr is now pointing to: %d, array[0] is: %d\n\n",
		                                        *ptr, array[0]);

	/*What will print?  Remember this is an array.
	 *The first option, *ptr + 3, says go to where ptr is pointing, get the
	 *value and add 3.  The second *(ptr + 3).  This is pointer arithmetic
	 *it says go three address down and and get the value.  Three addresses down
	 *is the 4th element of the array but index 3. Don't get this confused.   */
	printf("*ptr + 3 is: %d, array[0] + 3 is %d, array[3] is %d\n", *ptr + 3,
		                                       array[0] + 3 , array[3]);
	printf("*(ptr + 3) is: %d,  array[3] is %d\n", *(ptr + 3),
		                                             array[3]);

	/*Here I am printing the address of ptr + 3. This is to show you that the
	 *ptr is moving 3 elements down to element 4 index 3. Compare the address with the address printed
	 *for the element above.  */
	printf("address of ptr + 3 is indeed three elements from element 0:  %p\n\n",
		                                                     ptr + 3);

	/*Does the above change what ptr is pointing to?*/
	printf("*ptr is pointing to: %d\n\n", *ptr);

	/*Make the pointer point to the 2nd element using pointer arithmatic not array
	 *notation.  Array notation would have looked like this prt = &array[1] (2nd
	 *element index 1)*/
	ptr += 1;

	printf("*ptr is: %d which is the 2nd element of array.\n\n", *ptr);

	/*Now this proves where ptr is pointing has changed?  */

	printf("*ptr is: %d\n\n", *ptr);


	/*And just to show you this actually changes where the pointer is pointing
	 * lets do this again.*/
	ptr += 5;

	printf("*ptr is: %d, which is the 7th element index 6\n", *ptr);

	return 0;
}
