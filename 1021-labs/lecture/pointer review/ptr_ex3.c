#include <stdio.h>

int main()
{

    /*The next two lines of code are really bad practice.  
     *You should always initialize your variables especially a pointer.  */
    int *ptr;
    int x;

    /*ptr is being set to the address of x and then using the pointer, x is
    *being set to equal 0.  (*ptr - says go to were ptr is pointing, which is
    *the address of x, and set it equal to 0 --  therefore x is now equal to 0)*/
    ptr = &x;
    *ptr = 0;

    /*These should print the same value.  Why?*/
    printf(" x = %d\n", x);
    printf(" *ptr = %d\n", *ptr);

    /*This is using the pointer to change the value of the variable x.
     *This says go to where ptr is pointing and add 5 to the value*/
    *ptr += 5;
    /*Printing to make sure the above changed the value of x.*/
    printf(" x  = %d\n", x);
    printf(" *ptr = %d\n", *ptr);


    /*This says go to where pointer is pointing and increment the value by 1
     *remember i++ is the equivalent to i = i + 1.  */
    (*ptr)++;
    /*Printing to check the values.*/
    printf(" x = %d\n", x);
    printf(" *ptr = %d\n", *ptr);

    return 0;
}
