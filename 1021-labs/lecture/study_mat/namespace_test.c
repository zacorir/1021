/* Another example of namespaces both foo and bar have a function
*  called value.  The two value functions have different signatures
*  One has a return value of int and the other has a return value of double*/
#include <iostream>
using namespace std;

namespace foo{
  int value() { return 5;}
}

namespace bar{
  const double pi = 3.1416;
  double value() { return 2*pi; }
}

/*The main shows us how to call the function from each namespace*/
int main () {
  cout << foo::value() << '\n';
  cout << bar::value() << '\n';
  cout << bar::pi << '\n';
  return 0;
}
