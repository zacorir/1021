#include <stdio.h>
#include "sorting.h"
#include "string.h"

int main (int argc, char *argv[]) {
  char files[50];
  if (argc != 3){
    printf ("USAGE: ./SORT <file_to_sort> <type_of_sort>\n");
    return 1;
  }

  FILE* in_file = fopen (argv[1], "r");

  int data_size = 0;
  int num;

  while (fscanf (in_file, "%d", &num)==1) {
    data_size++;
  }

  fseek (in_file, 0, SEEK_SET);

  int data[data_size];

  for (int i = 0; i < data_size; i++) {
    fscanf (in_file, "%d", &data[i]);
  }
  if (strcmp (argv[2], "bubble") == 0) {
    bubble_sort (data, data_size);
  }
  if (strcmp (argv[2], "comb")== 0) {
    comb_sort (data, data_size);
  }

  sprintf (files, "%s_%d.dat", argv[2], data_size);

  FILE* out_file = fopen (files, "w");

  for (int i = 0; i < data_size; i++) {
    fprintf (out_file, "%d\n", data[i]);
  }
  fclose(out_file);
  fclose(in_file);

  return 0;
}
