#include <stdio.h>
#include "sorting.h"
#include "stdbool.h"

void bubble_sort(int* numbers, int size) {
  bool swapped = true;

  while (swapped == true) {
    swapped = false;
    for (int i = 0; i < size - 1; i++) {
      if (numbers[i] > numbers[i + 1]) {
        swapped = true;
        swap (&numbers[i], &numbers[i + 1]);
      }
    }
  }
}

void comb_sort(int* numbers, int size) {
  int gap = size;
  double shrink = 1.3;
  bool sorted = false;
  int i = 0;

  while (sorted == false) {
    gap = (gap / shrink);
    if (gap > 1) {
      sorted = false;
    }
    else {
      gap = 1;
      sorted  = true;
    }
    for(i = 0; i+gap < size; i++) {
      if (numbers[i] > numbers[i + gap]) {
        swap (&numbers[i], &numbers[i + gap]);
        sorted = false;
      }
    }
  }
}

void swap(int* left, int* right) {
  int temp;
  temp = *left;
  *left = *right;
  *right = temp;
}
