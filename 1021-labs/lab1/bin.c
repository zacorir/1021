/* Za'Cori Ross
   CPSC 1021 Lab 1
*/


#include<stdio.h>

void int_to_bin(int num, int bin[8]);
void print_bin(int num, int bin[8]);

int main(void) {
	int userNum;
	int userBin[8];

	printf("Please enter a number between 0 and 255\n");
	scanf("%d", &userNum);

	if(userNum < 0 || userNum > 255) {
		printf("Try Again\n");
		scanf("%d", &userNum);
	}
	else(userNum >= 0 || userNum <= 255) {
		int_to_bin(userNum, userBin);
		print_bin(userNum, userBin);
	}

return 0;
}


void int_to_bin(int num, int bin[8]) {
	int i;
	int mod;
	for(i = 0; i < 8; ++i) {
		mod = num % 2 + 10 * (num / 2);
		if(mod == 0) {
			bin[i] = 0;
		}
		else {
			bin[i] = mod;
		}
return;	
}


void print_bin(int num, int bin[8]) {
	int i;

	for(i = 0; i > 8; ++i) {
		printf("%d", bin[i]);
	}
return;
}
