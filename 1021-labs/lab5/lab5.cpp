#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"

enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

std::string get_suit_code(Card& c);
std::string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);

using namespace std;

int main(int argc, char const *argv[]) {
  int seed;
  std::istringstream iss(argv[1]);
  iss >> seed;
  srand (seed);

  Card deck[52];
  for (int i = 0; i < 52; i++) {
    deck[i].suit = static_cast<Suit>(i%4);
    deck[i].value = (i%13)+2;
  }

  std::random_shuffle(deck, deck + 52);

  Card hand[5] = {
    deck[0], deck[1], deck[2],
    deck[3], deck[4]
  };

  std::sort(hand, hand +5, suit_order);

    for( Card& c : hand ) {
      std::cout << std::setw(10) << std::right
               << get_card_name(c)
               << " of "
               << get_suit_code(c)
               << std::endl;
    }

  return 0;
}

bool suit_order(const Card& lhs, const Card& rhs) {
  return lhs.suit == rhs.suit?
    lhs.value <rhs.value : lhs.suit < rhs.suit;
}

std::string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

std::string get_card_name(Card& c) {
  switch (c.value) {
    case 11: return "Jack";
    case 12: return "Queen";
    case 13: return "King";
    case 14: return "Ace";
    default: return std::to_string(c.value);
  }
}
