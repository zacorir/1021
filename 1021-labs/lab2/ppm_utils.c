/*  Za'Cori Ross
    Cpsc 1021-01
    Lab #2
*/

#include"ppm_utils.h"

header_t read_header (FILE* image_file){
  scanf("%s", header.MAGIC_NUMBER);
  scanf("%d %d %d ", &header.WIDTH, &header.HEIGHT, &header.MAX_COLOR);
  return header;
}

image_t* read_ppm (FILE* image_file){
  header_t header = read_header(image_file);
  image_t* image = NULL;
  if (strcmp("P3", header.MAGIC_NUMBER) == 0) {
    image = read_p3(image_file, header);
  } else if (strcmp("P6", header.MAGIC_NUMBER) == 0) {
    image = read_p6(image_file, header);
  }
  return image;
}

image_t* read_p6 (FILE* image_file, header_t header){
  //num_pixels is local variable for WIDTH * HEIGHT
  int num_pixels = header.HEIGHT * header.WIDTH;
  image_t* image = (image_t*) malloc(sizeof(image_t));
  image->header.MAGIC_NUMBER[1];
  image->header = header;
  image->pixels = (pixel_t*) malloc(sizeof(pixel_t) * num_pixels);
  for(int i = 0; i < num_pixels; i++) {
    char r,g,b;//local variables that i cast into
    fscanf(image_file, "%c%c%c", &r, &g, &b);
    image->pixels[i].R = (int) r;
    image->pixels[i].G = (int) g;
    image->pixels[i].B = (int) b;
  }
  return image;
}

image_t* read_p3 (FILE* image_file, header_t header){
  int num_pixels = header.HEIGHT * header.WIDTH;
  image_t* image = (image_t*) malloc(sizeof(image_t));
  image->header.MAGIC_NUMBER[1];
  image->header = header;
  image->pixels = (pixel_t*) malloc(sizeof(pixel_t) * num_pixels);
  for(int i = 0; i < num_pixels; i++) {
    fscanf(image_file, "%d %d %d ", &pixel.R, &pixel.G, &pixel.B);
  }
  return image;

}

void write_header (FILE* out_file, header_t header){
  scanf("%s ", header.MAGIC_NUMBER);
  scanf("%d %d %d ", header.HEIGHT, header.WIDTH, header.MAX_COLOR);
}

void write_p6 (FILE* out_file, image_t* image){
  int num_pixels = header.HEIGHT * header.WIDTH;
  header.MAGIC_NUMBER[1] = "6";
  write_header(out_file, header);
  image->header.HEIGHT;
  image->header.WIDTH;
  char r,b,g;
  for(int i = 0; i < num_pixels; i++){
    printf("%c%c%c", r, g, b);
    image->pixels[i].R = (char) r;
    image->pixels[i].G = (char) g;
    image->pixels[i].B = (char) b;
  }
}

void write_p3 (FILE* out_file, image_t* image){
  header.MAGIC_NUMBER[1] = "3";
  write_header(out_file, header);
  image->header.HEIGHT;
  image->header.WIDTH;
  char r,b,g;
  printf("%d %d %d ", r, g, b);
}
