/*  Za'Cori Ross
    Cpsc 1021-01
    Lab #2
*/

#include"ppm_utils.h"

int main (int argc, char *argv[]){
  if(argc != 3){
    printf("Incorrect command line arguments");
    return 1;
  }

  FILE *input = fopen(argv[0], "r");
  if(input == 0){
    printf("Could not open file\n");
    return 1;
  }

  image_t* img = read_ppm(input);

  FILE *output = fopen(argv[1], "w");
  if (output == 0){
    printf("(Could not open file\n)");
    return 1;
  }

  write_p6(output, img);

  free(img->pixels);
  free(img);
  fclose(input);
  fclose(output);
  return 0;
}
